import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import * as moment from 'moment';
/**
 * Generated class for the DetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail',
  templateUrl: 'detail.html',
})
export class DetailPage {

  lamp: any = {};
  type: any = 0;
  auto: any = 0;
  options: any = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public db: AngularFireDatabase) {
    this.lamp = navParams.get('lamp');    
  }

  ionViewDidLoad() {
    let tipe = this.lamp.name + '/tipe';
    let state = this.lamp.name + '/state';
    let auto = this.lamp.name + '/auto';
    this.db.object(auto).valueChanges().subscribe(
      val => {
        console.log(val);
        this.auto = val;
      }
    ); 
    this.db.object(tipe).valueChanges().subscribe(
      val => {
        console.log(val);
        this.type = val;
      }
    ); 
    this.db.object(state).valueChanges().subscribe(
      val => {
        console.log(val);
        this.options = val;
      }
    ); 
  }

  changeMode(ev: any) {
    if (ev.checked) {
      this.db.object(this.lamp.name + '/auto').set(1);
    } else {
      this.db.object(this.lamp.name + '/auto').set(0);
    }
  }  

  changeType(ev: any){
    let value = parseInt(ev);
    this.db.object(this.lamp.name + '/tipe').set(value);
  }

  changeValue(ev: any){
    let value = parseInt(ev);
    let name = this.lamp.name + '/log';
    let log = this.db.list(name);
    switch (value) {
      case 0:
          log.push({kondisi: 'Off', tanggal: moment().format('YYYY-MM-DD HH:mm:ss')});
          this.db.object(this.lamp.name + '/state').set(value);
          this.db.object(this.lamp.name + '/relay1').set(0);
          this.db.object(this.lamp.name + '/relay2').set(0);
          this.db.object(this.lamp.name + '/relay3').set(0);
          this.db.object(this.lamp.name + '/relay4').set(0);
        break;
      case 1:
          log.push({ kondisi: 'Dim', tanggal: moment().format('YYYY-MM-DD HH:mm:ss') });
          this.db.object(this.lamp.name + '/state').set(value);
          this.db.object(this.lamp.name + '/relay1').set(0);
          this.db.object(this.lamp.name + '/relay2').set(0);
          this.db.object(this.lamp.name + '/relay3').set(1);
          this.db.object(this.lamp.name + '/relay4').set(0);
        break;
      case 2:
          log.push({ kondisi: 'Half Bright', tanggal: moment().format('YYYY-MM-DD HH:mm:ss') });
          this.db.object(this.lamp.name + '/state').set(value);
          this.db.object(this.lamp.name + '/relay1').set(0);
          this.db.object(this.lamp.name + '/relay2').set(0);
          this.db.object(this.lamp.name + '/relay3').set(0);
          this.db.object(this.lamp.name + '/relay4').set(1);
        break;
      case 3:
          log.push({ kondisi: 'Bright', tanggal: moment().format('YYYY-MM-DD HH:mm:ss') });
          this.db.object(this.lamp.name + '/state').set(value);
          this.db.object(this.lamp.name + '/relay1').set(1);
          this.db.object(this.lamp.name + '/relay2').set(1);
          this.db.object(this.lamp.name + '/relay3').set(1);
          this.db.object(this.lamp.name + '/relay4').set(1);
        break;
    
      default:
        break;
    }
    
  }

}
