import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DetailPage } from '../detail/detail';
import { AngularFireDatabase } from 'angularfire2/database';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public auto: any = 1;  
  public lamps: any = [];

  site = {
    coba: 'oke'
  }

  constructor(
    public navCtrl: NavController,
    public db: AngularFireDatabase
  ) {
    // this.db.list('state_lamp1').push(this.site);
    // this.db.object('state_lamp1').set(1);
  }

  ionViewDidLoad(){

    this.lamps = [
      {title: 'Lampu A', status: 'off', name: 'lampuA'},
      {title: 'Lampu B', status: 'off', name: 'lampuB'},
      {title: 'Lampu C', status: 'off', name: 'lampuC'},
      {title: 'Lampu D', status: 'off', name: 'lampuD'},
      // {title: 'Lampu E', status: 'off', name: 'lampuE'},
      // {title: 'Lampu F', status: 'off', name: 'lampuF'}
    ];

  }

  changeMode(ev: any){
    if(ev.checked){
      this.db.object('auto').set(1);
    }else{
      this.db.object('auto').set(0);
    }
    
  }

  itemSelected(value){
    this.navCtrl.push(DetailPage, {lamp: value});
  }

}
